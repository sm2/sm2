import name.li.sm.model.api {
	MovesSupplier
}
import name.li.sm.model.players {
	User
}
import ceylon.promise {
	Promise
}
import name.li.sm.model.world {
	WorldState,
	Move,
	WorldStateWrapper,
	ownedPlanetPredicate
}
import name.li.sm.logging {
	LoggingSupport
}

class Megatron(User me) satisfies LoggingSupport  {

	shared {Move*} getMoves(WorldState ws) {
		value balancedWorldState = balance(ws);
		
		return {};
	}

	shared WorldState balance(WorldState worldState) { 
		value wsw = WorldStateWrapper(worldState);
		
		
		value ownedPlanets = wsw.getPlanets(ownedPlanetPredicate(me));
		
		
		return worldState;
	}
	
	
}