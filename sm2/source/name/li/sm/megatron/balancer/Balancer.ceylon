import name.li.sm.model.players {
	User
}
import name.li.sm.model.world {
	WorldState,
	WorldStateWrapper
}
import name.li.sm.game.ai.solver {
	Solver,
	PlanetSolver
}
import name.li.sm.model.planets {
	PlanetState,
	Planet
}

abstract class BalancingAlgo() of broad | deep {
}
object deep extends BalancingAlgo() {}
object broad extends BalancingAlgo() {}

class Balancer(User me, BalancingAlgo algo = deep) {
	
	shared WorldState balance(WorldState ws) {
		value wsw = WorldStateWrapper(ws);
		value solver = Solver(ws);
		
		value planetsToBalance = solver.getOwnedPlanetsOrdererByProductivity(me);
		
		for (ps in planetsToBalance) {
			value planetLayers = solver.getLayeredPlanetsFromRoot(me, ps.planet);
			value balancedWorldState = balancePlanet(wsw, ps, planetLayers);
		}
		return ws;
	}
	
	shared void balancePlanet(WorldStateWrapper wsw, PlanetState ps, Map<Planet,Integer> layers) {
		value solver = Solver(wsw.worldState);
		value planetSolver = solver.forPlanet(ps);
		if (exists requiredFleet = planetSolver.shipsRequiredForBestGrowth()) {
			value request = Request(requiredFleet, ps);
			handleFleetRequest(request, wsw, layers);
		}
		solver.layeredPlanets(me);
		wsw.getConnectedPlanets(ps.planet);
	}
	
	void handleFleetRequest(Request request, WorldStateWrapper wsw, Map<Planet,Integer> layers) {
		value solver = Solver(wsw.worldState);
		value planetState = request.requestor;
		value planet = planetState.planet;
		
		value requestAfterUsingCurrentPlanetResources = accepFleetRequest(request, wsw, layers, planetState);
		
		value connectedPlanets = wsw.connectedPlanets(planet);
		value requestorlayer = layers.get(planet);
		assert (exists requestorlayer);
		for (connectedPlanet in connectedPlanets) {
			value layer = layers.get(connectedPlanet);
			assert (exists layer);
			if (layer > requestorlayer) {
			}
		}
	}
	
	void accepFleetRequest(Request request, WorldStateWrapper wsw, Map<Planet,Integer> tiers, PlanetState acceptor) {
		value solver = Solver(wsw.worldState);
		value planetSolver = solver.forPlanet(acceptor);
		variable value result = request;

		// try local ships first
		if (exists availableFleet = planetSolver.shipsPossibleToDetachDueToOverGrowth()) {
			result = request.fulfill(acceptor, availableFleet, "Overgrowth");
		}

		if (solver.isBetter(request.requestor.planet, acceptor.planet)) {
		}
	}
}
