import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	PlanetState
}
import name.li.sm.model.world {
	Move
}

class Request(
	shared FleetSize requestedSize,
	shared PlanetState requestor,
	Request? originalRequest = null,
	{Move*} moves = {},
	String? comment = null) {
	
	shared Request fulfill(PlanetState replier, FleetSize availableSize, String comment) {
		value yetRequiredSize = requestedSize.subtract(availableSize);
		assert (exists yetRequiredSize);
		return Request(
			yetRequiredSize,
			requestor,
			this,
			moves.follow(Move(replier.owner, replier.planet, requestor.planet, availableSize)),
			comment
		);
	}
	
	shared Request nest(PlanetState newRequestor) {
		return Request(
			requestedSize,
			newRequestor,
			this,
			moves, "requestor changed");
	}
	
	shared Boolean isFulfilled() {
		return requestedSize.equals(FleetSize(0));
	}
}
