import ceylon.collection {
	MutableList,
	LinkedList,
	HashSet
}

import java.util {
	Random
}

import name.li.sm.game.ai.solver {
	Solver
}
import name.li.sm.model.planets {
	Planet
}
import name.li.sm.model.players {
	User
}
import name.li.sm.model.world {
	Move,
	WorldState,
	WorldStateWrapper
}
shared class SimpleAi(shared User player) {

	shared {Move*} getMoves(WorldState ws) {
		value wsw = WorldStateWrapper(ws);
		value solver = Solver(ws);
		MutableList<Move> moves = LinkedList<Move>();
		value planetLayers = solver.layeredPlanets(player);
		value nonTargetablePlanets = HashSet<Planet>();
		for (layer in planetLayers.reversed) {
			nonTargetablePlanets.addAll(layer);
			for (planet in layer) {
				value possibleAttackTargets = wsw.getConnectedPlanets(planet).filter((e) {
					 return wsw.getPlanetState(e).owner != player && !nonTargetablePlanets.contains(e);
				});
				if (!possibleAttackTargets.empty) {
					native("jvm") value index = Random().nextInt(possibleAttackTargets.size);
					value targetPlanet = possibleAttackTargets.skip(index).first;
					if (exists targetPlanet) {
						if (exists availableFleet = solver.forPlanet(planet).shipsPossibleToDetachDueToOverGrowth()) {
							moves.add(Move(player, planet, targetPlanet, availableFleet));
						}
					}
				}
			}
		}
		return moves;
	}
}