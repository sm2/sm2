import name.li.sm.model.world {
	WorldState,
	WorldStateBuilder,
	WorldBuilder
}
import name.li.sm.model.players {
	NormalPlayer,
	neutralPlayer
}
import name.li.sm.model.planets {
	Planet,
	LoggerTicker,
	ProportionalFleetSizeBuilder,
	FleetMovementsResolver,
	BattleResolver,
	PlanetState
}
import name.li.sm.model.fleets {
	FleetSize,
	Fleet
}
import ceylon.test {
	test,
	assertEquals,
	assertTrue,
	assertFalse
}
class SolverTests() {
	
	value auir = Planet("Auir", FleetSize(1000));
	value terra = Planet("Terra", FleetSize(1000));
	value zerus = Planet("Zerus", FleetSize(1000));
	value char = Planet("Char", FleetSize(500));
	value zerus2 = Planet("Zerus2", FleetSize(100));
	value zerus3 = Planet("Zerus3", FleetSize(100));
	
	value zerg = NormalPlayer("Zerg");
	value terran = NormalPlayer("Terran");
	value protoss = NormalPlayer("Protoss");
	
	value builder = ProportionalFleetSizeBuilder(1.0);
	value ticker = FleetMovementsResolver().chain(BattleResolver()).chain(builder);
	
	value world = WorldBuilder(ticker)
		.withPlayers(zerg, terran, protoss)
		.withPlanet(char).connectedTo(zerus, terra)
		.withPlanet(auir).connectedTo(terra)
		.withPlanet(zerus).connectedTo(terra)
		.withPlanet(zerus2).connectedTo(zerus)
		.withPlanet(zerus3).connectedTo(zerus2)
		.build();
	
	value initialWorldState = WorldStateBuilder(world, neutralPlayer)
		.withPlayer(protoss).havingFleet(auir, FleetSize(100))
		.withPlayer(zerg).havingFleet(zerus, FleetSize(500))
		.withPlayer(zerg).havingFleet(zerus2, FleetSize(500))
		.withPlayer(zerg).havingFleet(zerus3, FleetSize(500))
		.build();
	
	test
	shared void testPlanetLayers() {
		value solver = Solver(initialWorldState);
		value zergPlanets = solver.layeredPlanets(zerg);
		value layer1 = zergPlanets.first;
		value layer2 = zergPlanets.rest.first;
		value layer3 = zergPlanets.rest.rest.first;
		assertEquals(layer1?.sequence(), [zerus]);
		assertEquals(layer2?.sequence(), [zerus2]);
		assertEquals(layer3?.sequence(), [zerus3]);
	}
	
	test
	shared void testIndexedPlanetLayers() {
		value solver = Solver(initialWorldState);
		value planets = solver.getLayeredPlanetsFromRoot(zerg, zerus);
		assertEquals(planets.get(zerus), 1);
		assertEquals(planets.get(zerus2), 2);
		assertEquals(planets.get(zerus3), 3);
	}
	
	test
	shared void testIsBetterChoosesMoreProdutiveWorld() {
		value solver = Solver(initialWorldState);
		assertTrue(solver.isBetter(auir, char));
		assertTrue(solver.isBetter(char, zerus2));
		assertFalse(solver.isBetter(char, auir));
		assertFalse(solver.isBetter(char, terra));
	}
}
