import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	BattleResolver
}
class BattleResolverUnticker(BattleResolver br) extends AbstractUnticker<BattleResolver>(br) {
	
	shared actual FunctionalPlanetState untick(FunctionalPlanetState previousState, FunctionalPlanetState nextState) {
		value states = [previousState, nextState];
		value incomingOwnerFleetSize = chooseGoodSlot(states*.incomingOwnerFleetSize);
		value incomingEnemyFleetSize = chooseGoodSlot(states*.incomingEnemyFleetSize);
		variable Slot<FleetSize> resultOwnerFleetSize = previousState.ownerFleetSize;
		variable Slot<FleetSize> resultEnemyFleetSize = previousState.enemyFleetSize;
		value prevOwner = previousState.owner.evaluate();
		value nextOwner = nextState.owner.evaluate();
		assert (exists prevOwner);
		assert (exists nextOwner);
		value ownerChanged = prevOwner != nextOwner;
		
		
		variable Slot<FleetSize> calculatedResultOwnerFleetSize = BadSlot();
		if (ownerChanged) {
			calculatedResultOwnerFleetSize = subtractFleetSizeSlots(previousState.enemyFleetSize, nextState.ownerFleetSize);
		} else {
			calculatedResultOwnerFleetSize = addFleetSizeSlots(previousState.enemyFleetSize, nextState.ownerFleetSize);
		}
		resultOwnerFleetSize = chooseGoodSlot([resultOwnerFleetSize, calculatedResultOwnerFleetSize]);
		
		variable Slot<FleetSize> calculatedResultEnemyFleetSize = BadSlot();
		if (ownerChanged) {
			calculatedResultEnemyFleetSize = addFleetSizeSlots(nextState.ownerFleetSize, previousState.ownerFleetSize);
		} else {
			calculatedResultEnemyFleetSize = subtractFleetSizeSlots(previousState.ownerFleetSize, nextState.ownerFleetSize);
		}
		resultEnemyFleetSize = chooseGoodSlot([resultEnemyFleetSize, calculatedResultEnemyFleetSize]);
		
		return FunctionalPlanetState(
			resultOwnerFleetSize,
			resultEnemyFleetSize,
			incomingOwnerFleetSize,
			incomingEnemyFleetSize,
			previousState.owner,
			nextState.planet);
	}
}
