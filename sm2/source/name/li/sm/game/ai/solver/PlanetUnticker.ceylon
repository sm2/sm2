interface PlanetUnticker {
	shared formal FunctionalPlanetState untick(FunctionalPlanetState previousState, FunctionalPlanetState nextState);
}