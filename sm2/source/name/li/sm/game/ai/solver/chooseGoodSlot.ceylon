Slot<Type> chooseGoodSlot<Type>(Slot<Type>[] slots) {
	return slots.find((Slot<Type> elem) => !elem is BadSlot) else BadSlot();
}
