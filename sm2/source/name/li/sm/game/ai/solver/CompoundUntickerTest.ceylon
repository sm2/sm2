import name.li.sm.model.fleets {
	Fleet,
	FleetSize
}
import name.li.sm.model.planets {
	Planet,
	PlanetState,
	FleetMovementsResolver,
	ProportionalFleetSizeBuilder,
	BattleResolver
}
import name.li.sm.model.players {
	NormalPlayer
}
import ceylon.test {
	assertEquals,
	test
}
class CompoundUntickerTest() {
	value ticker = FleetMovementsResolver()
			.chain(ProportionalFleetSizeBuilder(0.5))
			.chain(BattleResolver());
	value unticker = getCompoundUnticker(*ticker.tickers);
	
	value planet = Planet("foo", FleetSize(100));
	value owner = NormalPlayer("user");
	value enemy = NormalPlayer("enemy");
	
	value basicSize = FleetSize(10);
	value incomingSize = FleetSize(30);
	value incomingEnemySize = FleetSize(20);
	
	variable value ps = PlanetState(owner, planet);
	ps = ps.addFleet(Fleet(owner, basicSize))
			.addIncomingFleet(Fleet(enemy, incomingEnemySize))
			.addIncomingFleet(Fleet(owner, incomingSize));
	
	test
	shared void testFleetBuilderUnticker() {
		value desiredPlanetState = ps.removeFleetForUser(owner).addFleet(Fleet(owner, FleetSize(40)));
		value untickedState = unticker.untick(newFunctionalPlanetStateFromPlanetState(ps), newFunctionalPlanetStateFromPlanetState(desiredPlanetState));
		assertEquals(untickedState.ownerFleetSize.evaluate(), FleetSize(10));
	}
	
}