interface Slot<out Type> {
	shared formal Type? evaluate();
}