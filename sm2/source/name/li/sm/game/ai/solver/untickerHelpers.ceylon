
import name.li.sm.model.planets {
	PlanetTicker,
	FleetMovementsResolver,
	BattleResolver,
	ProportionalFleetSizeBuilder,
	CompoundPlanetTicker,
	LoggerTicker
}

class NopUnticker() satisfies PlanetUnticker {
	shared actual FunctionalPlanetState untick(FunctionalPlanetState previousState, FunctionalPlanetState nextState) => nextState;
}

PlanetUnticker getUnticker(PlanetTicker ticker) {
	if (ticker is FleetMovementsResolver) {
		return MovementsResolverUnticker();
	} else if (is BattleResolver ticker) {
		return BattleResolverUnticker(ticker);
	} else if (is ProportionalFleetSizeBuilder ticker) {
		return FleetBuilderUnticker(ticker);
	} else if (is CompoundPlanetTicker ticker) {
		return CompoundUnticker(*ticker.tickers.map(getUnticker));
	} else if(is LoggerTicker ticker) {
		return NopUnticker();
	} else {
		throw Exception("Unknown planet ticker ``ticker``");
	}
}


CompoundUnticker getCompoundUnticker(PlanetTicker* tickers) {
	return CompoundUnticker(*tickers.map(getUnticker));
}