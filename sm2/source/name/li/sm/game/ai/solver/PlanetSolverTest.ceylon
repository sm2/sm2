import ceylon.test {
	test,
	assertEquals,
	assertNotNull,
	assertNull
}

import name.li.sm.model.fleets {
	FleetSize,
	Fleet
}
import name.li.sm.model.planets {
	Planet,
	ProportionalFleetSizeBuilder,
	FleetMovementsResolver,
	BattleResolver
}
import name.li.sm.model.players {
	NormalPlayer,
	neutralPlayer
}
import name.li.sm.model.world {
	WorldStateBuilder,
	WorldBuilder
}
class PlanetSolverTest() {
	
	
	value auir = Planet("Auir", FleetSize(1000));
	value terra = Planet("Terra", FleetSize(1000));
	value zerus = Planet("Zerus", FleetSize(1000));
	value char = Planet("Char", FleetSize(500));
	
	value zerg = NormalPlayer("Zerg");
	value terran = NormalPlayer("Terran");
	value protoss = NormalPlayer("Protoss");
	
	
	value builder = ProportionalFleetSizeBuilder(1.0);
	value ticker = FleetMovementsResolver().chain(BattleResolver()).chain(builder);
	value unticker = CompoundUnticker(FleetBuilderUnticker(builder), BattleResolverUnticker(BattleResolver()), MovementsResolverUnticker());
	
	value world = WorldBuilder(ticker)
			.withPlayers(zerg, terran, protoss)
			.withPlanet(char).connectedTo(zerus, terra)
			.withPlanet(auir).connectedTo(terra)
			.withPlanet(zerus).connectedTo(terra)
			.build();
	
	value initialWorldState = WorldStateBuilder(world, neutralPlayer)
			.withPlayer(protoss).havingFleet(auir, FleetSize(100))
			.withPlayer(zerg).havingFleet(zerus, FleetSize(500))
			.build();
	
	
	test
	shared void testShipsPossibleToDetachDueToOverGrowth() {
		value auirPs = initialWorldState.planetStates.find((ps) => ps.planet == auir);
		assert(exists auirPs);
		
		value solver1 = PlanetSolverImpl(ticker, unticker, auirPs);
		value size1 = solver1.shipsPossibleToDetachDueToOverGrowth();
		assertNull(size1);
		
		value solver2 = PlanetSolverImpl(ticker, unticker, auirPs.removeFleetForUser(auirPs.owner).addFleet(Fleet(auirPs.owner, FleetSize(950))));
		value size2 = solver2.shipsPossibleToDetachDueToOverGrowth();
		assertEquals(size2, FleetSize(450));
	}
	
	test
	shared void testShipsRequiredForBestGrowth() {
		value auirPs = initialWorldState.planetStates.find((ps) => ps.planet == auir);
		assert(exists auirPs);
		
		value solver1 = PlanetSolverImpl(ticker, unticker, auirPs);
		value size1 = solver1.shipsRequiredForBestGrowth();
		assertEquals(size1, FleetSize(400));
	}
	
	test
	shared void testDetachingAllShipsExceptGarrison() {
		value auirPs = initialWorldState.planetStates.find((ps) => ps.planet == auir);
		assert(exists auirPs);
		
		value solver1 = PlanetSolverImpl(ticker, unticker, auirPs);
		value size1= solver1.allAvaiableShipsExceptGarrison();
		assertEquals(size1, FleetSize(99));
		
		value solver2 = PlanetSolverImpl(ticker, unticker, auirPs.removeFleetForUser(auirPs.owner).addIncomingFleet(Fleet(zerg, FleetSize(50))));
		value size2 = solver2.allAvaiableShipsExceptGarrison();
		assertEquals(size2, FleetSize(49));

		value solver3 = PlanetSolverImpl(ticker, unticker, auirPs.removeFleetForUser(auirPs.owner).addIncomingFleet(Fleet(zerg, FleetSize(100))));
		value size3 = solver3.allAvaiableShipsExceptGarrison();
		assertNull(size3);
	}
	
	
}