import ceylon.test {
	test,
	assertEquals
}

import name.li.sm.model.fleets {
	Fleet,
	FleetSize
}
import name.li.sm.model.planets {
	Planet,
	PlanetState,
	ProportionalFleetSizeBuilder
}
import name.li.sm.model.players {
	NormalPlayer
}
class FleetBuilderUntickerTest() {
	
	value ticker = ProportionalFleetSizeBuilder(0.5);
	value unticker = FleetBuilderUnticker(ticker);
	
	value planet = Planet("foo", FleetSize(100));
	value owner = NormalPlayer("user");
	value enemy = NormalPlayer("enemy");
	
	value basicSize = FleetSize(10);
	value incomingSize = FleetSize(30);
	value incomingEnemySize = FleetSize(20);
	
	variable value ps = PlanetState(owner, planet);
	ps = ps.addFleet(Fleet(owner, basicSize))
			.addIncomingFleet(Fleet(enemy, incomingEnemySize))
			.addIncomingFleet(Fleet(owner, incomingSize));

	test
	shared void testFleetBuilderUnticker() {
		value desiredPlanetState = ps.removeFleetForUser(owner).addFleet(Fleet(owner, FleetSize(90)));
		value untickedState = unticker.untick(newFunctionalPlanetStateFromPlanetState(ps), newFunctionalPlanetStateFromPlanetState(desiredPlanetState));
		assertEquals(untickedState.ownerFleetSize.evaluate(), FleetSize(60));
	}
	
	
}