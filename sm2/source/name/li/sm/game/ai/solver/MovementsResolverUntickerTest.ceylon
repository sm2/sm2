import ceylon.test {
	test,
	assertEquals
}
import name.li.sm.model.fleets {
	FleetSize,
	Fleet
}
import name.li.sm.model.planets {
	FleetMovementsResolver,
	PlanetState,
	Planet
}
import name.li.sm.model.players {
	NormalPlayer
}
class MovementsResolverUntickerTest() {
	
	value ticker = FleetMovementsResolver();
	value unticker = MovementsResolverUnticker();
	
	value planet = Planet("foo", FleetSize(100));
	value owner = NormalPlayer("user");
	value enemy = NormalPlayer("enemy");
	
	value basicSize = FleetSize(10);
	value incomingSize = FleetSize(30);
	value incomingEnemySize = FleetSize(20);
	
	variable value ps = PlanetState(owner, planet);
	ps = ps.addFleet(Fleet(owner, basicSize))
		.addIncomingFleet(Fleet(enemy, incomingEnemySize))
		.addIncomingFleet(Fleet(owner, incomingSize));
	value psNext = ticker.tick(ps);
	
	assertEquals(psNext.ownerFleet.size, basicSize.add(incomingSize));
	assertEquals(psNext.fleets.getFleet(enemy)?.size, incomingEnemySize);
	
	test
	shared void testUntickKnownStateMove() {
		
		value fpsPrev = newFunctionalPlanetStateFromPlanetState(ps);
		value fpsNext = newFunctionalPlanetStateFromPlanetState(psNext);
		value untickedFps = unticker.untick(fpsPrev, fpsNext);
		
		assertEquals(untickedFps.ownerFleetSize.evaluate(), basicSize);
		assertEquals(untickedFps.incomingOwnerFleetSize.evaluate(), incomingSize);
		assertEquals(untickedFps.incomingEnemyFleetSize.evaluate(), incomingEnemySize);
	}
	
	test
	shared void testUntickPartialState() {
		value desiredResultFleetSize = FleetSize(99);
		value expectedBaseSize = desiredResultFleetSize.subtract(incomingSize);
		assert (exists expectedBaseSize);
		value fpsPrev = newFunctionalPlanetStateFromPlanetState(ps).with { ownerFleetSize = BadSlot(); };
		value fpsNext = newFunctionalPlanetStateFromPlanetState(psNext).with { ownerFleetSize = FuncSlot(() => desiredResultFleetSize); };
		value untickedFps = unticker.untick(fpsPrev, fpsNext);
		
		assertEquals(expectedBaseSize, untickedFps.ownerFleetSize.evaluate());
	}
}
