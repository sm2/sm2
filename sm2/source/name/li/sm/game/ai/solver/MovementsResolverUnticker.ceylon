import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	FleetMovementsResolver
}
class MovementsResolverUnticker() extends AbstractUnticker<FleetMovementsResolver>(FleetMovementsResolver()) {
	shared actual FunctionalPlanetState untick(FunctionalPlanetState previousState, FunctionalPlanetState nextState) {
		// TODO: sanity check for next state incoming fleets
		value enemySlotPair = resolvePair(previousState.incomingEnemyFleetSize, previousState.enemyFleetSize, nextState.enemyFleetSize);
		value ownerSlotPair = resolvePair(previousState.incomingOwnerFleetSize, previousState.ownerFleetSize, nextState.ownerFleetSize);
		value owner = chooseGoodSlot([previousState, nextState]*.owner);
		return FunctionalPlanetState(
			ownerSlotPair.first,
			enemySlotPair.first,
			ownerSlotPair.rest.first,
			enemySlotPair.rest.first,
			owner,
			nextState.planet);
	}
	
	[Slot<FleetSize>, Slot<FleetSize>] resolvePair(Slot<FleetSize> prevIncoming, Slot<FleetSize> prevExisting, Slot<FleetSize> nextExisting) {
		variable Slot<FleetSize> resultIncoming = BadSlot();
		variable Slot<FleetSize> resultExising = BadSlot();
		
		if (exists prevInFs = prevIncoming.evaluate()) {
			if (exists prevExFs = prevExisting.evaluate()) {
				resultIncoming = prevIncoming;
				resultExising = prevExisting;
			}
		}
		
		if (exists prevInFs = prevIncoming.evaluate()) {
			if (exists nextExFs = nextExisting.evaluate()) {
				resultIncoming = prevIncoming;
				resultExising = FuncSlot(() => FleetSize(nextExFs.integerValue - prevInFs.integerValue));
			}
		}
		
		if (exists prevExFs = prevExisting.evaluate()) {
			if (exists nextExFs = nextExisting.evaluate()) {
				resultIncoming = FuncSlot(() => FleetSize(nextExFs.integerValue - prevExFs.integerValue));
			}
		}
		
		return [resultExising, resultIncoming];
	}
}
