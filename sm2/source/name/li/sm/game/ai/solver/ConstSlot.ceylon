class ConstSlot<Type>(Type val) satisfies Slot<Type> {
	shared actual Type evaluate() => val;
}