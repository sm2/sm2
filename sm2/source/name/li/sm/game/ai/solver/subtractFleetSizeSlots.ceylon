import name.li.sm.model.fleets {
	FleetSize
}
Slot<FleetSize> subtractFleetSizeSlots(Slot<FleetSize> slot1, Slot<FleetSize> slot2) {
	if (exists val1 = slot1.evaluate()) {
		if (exists val2 = slot2.evaluate()) {
			return ConstSlot(FleetSize(val1.integerValue - val2.integerValue));
		}
	}
	return BadSlot();
}