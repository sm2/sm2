import name.li.sm.model.fleets {
	FleetSize,
	FleetsMap,
	Fleet
}
import name.li.sm.model.planets {
	PlanetState,
	PlanetTicker
}

shared PlanetSolver newPlanetSolver(PlanetTicker ticker, PlanetState ps) {
	return PlanetSolverImpl(ticker, getUnticker(ticker), ps);
}

class PlanetSolverImpl(PlanetTicker ticker, PlanetUnticker unticker, PlanetState ps) satisfies PlanetSolver {
	value fullyPopulatedState = ps.forkWith { fleets = FleetsMap().addFleet(Fleet(ps.owner, ps.planet.maxFleetSize)); };
	
	shared actual FleetSize? shipsPossibleToDetachDueToOverGrowth() {
		value initialState = ps.forkWith { fleets = ps.fleets.removeFleetForUser(ps.owner); };
		value resultState = unticker.untick(
			newFunctionalPlanetStateFromPlanetState(initialState).with { ownerFleetSize = BadSlot(); },
			newFunctionalPlanetStateFromPlanetState(fullyPopulatedState));
		value requiredFleetSize = resultState.ownerFleetSize.evaluate();
		assert (exists requiredFleetSize);
		value initialFleetSize = ps.ownerFleet.size;
		
		if (exists subResult = initialFleetSize.subtract(requiredFleetSize)) {
			return subResult;
		} else {
			return null;
		}
	}
	
	shared actual FleetSize? shipsRequiredForBestGrowth() {
		value resultState = unticker.untick(
			newFunctionalPlanetStateFromPlanetState(ps).with { ownerFleetSize = BadSlot(); },
			newFunctionalPlanetStateFromPlanetState(fullyPopulatedState));
		value requiredFleetSize = resultState.ownerFleetSize.evaluate();
		assert (exists requiredFleetSize);
		value initialFleetSize = ps.ownerFleet.size;
		
		if (exists subResult = requiredFleetSize.subtract(initialFleetSize)) {
			return subResult;
		} else {
			return null;
		}
	}
	
	shared actual FleetSize? allAvaiableShipsExceptGarrison() {
		value deltaState = unticker.untick(
			newFunctionalPlanetStateFromPlanetState(ps)
				.with { ownerFleetSize = ConstSlot(FleetSize(0)); incomingFriendlyFleetSize = BadSlot(); },
			newFunctionalPlanetStateFromPlanetState(ps.removeFleetForUser(ps.owner).addFleet(Fleet(ps.owner, FleetSize(1))))
				.with { incomingFriendlyFleetSize = BadSlot(); });
		
		if (exists garrisionFleetRequired = deltaState.incomingOwnerFleetSize.evaluate()) {
			return FleetSize(ps.ownerFleet.size.integerValue - garrisionFleetRequired.integerValue);
		} else {
			return null;
		}
	}
}
