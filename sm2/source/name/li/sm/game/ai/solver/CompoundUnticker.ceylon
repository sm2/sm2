class CompoundUnticker(PlanetUnticker* untickers) satisfies PlanetUnticker {

	shared actual FunctionalPlanetState untick(FunctionalPlanetState previousState, FunctionalPlanetState nextState) {
		return untickers.fold(nextState)((prevNext, unticker) => unticker.untick(previousState, prevNext));
	}
	
}