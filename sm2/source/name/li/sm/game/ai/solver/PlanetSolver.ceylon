import name.li.sm.model.fleets {
	FleetSize
}

shared interface PlanetSolver {

	shared formal FleetSize? shipsPossibleToDetachDueToOverGrowth();
	
	shared formal FleetSize? shipsRequiredForBestGrowth();
	
	shared formal FleetSize? allAvaiableShipsExceptGarrison();

}