import ceylon.collection {
	HashSet,
	MutableMap,
	HashMap,
	ArrayList
}

import name.li.sm.model.planets {
	Planet,
	PlanetState,
	planetStateToPlanet
}
import name.li.sm.model.players {
	User
}
import name.li.sm.model.world {
	WorldState,
	WorldStateWrapper,
	borderPlanetsPredicate,
	ownedPlanetPredicate
}

shared class Solver(WorldState ws) {
	value ticker = ws.world.ticker;
	value wsw = WorldStateWrapper(ws);
	value planetEvaluator = PlanetProductivityEvaluator({});
	PlanetState resolvePlanet(Planet|PlanetState planet) {
		if (is Planet planet) {
			return wsw.getPlanetState(planet);
		} else {
			return planet;
		}
	}
	shared PlanetSolver forPlanet(Planet|PlanetState planet) {
		return newPlanetSolver(ticker, resolvePlanet(planet));
	}
	shared [[Planet*]*] layeredPlanets(User perspective) {
		variable value borderPlanets = wsw.getPlanets(borderPlanetsPredicate(perspective)).map(planetStateToPlanet);
		variable [[Planet*]*] result = [borderPlanets.sequence()];
		variable value allPlayerPlanets = wsw.getPlanets(ownedPlanetPredicate(perspective)).map(planetStateToPlanet);
		value planetsToCheck = HashSet { *allPlayerPlanets };
		planetsToCheck.removeAll(borderPlanets);
		
		variable value nextLayer = borderPlanets;
		while (!planetsToCheck.empty) {
			value allConnectedPlanets = HashSet { *nextLayer.flatMap(wsw.getConnectedPlanets) };
			nextLayer = allConnectedPlanets.intersection(planetsToCheck);
			result = result.chain([nextLayer.sequence()]).sequence();
			planetsToCheck.removeAll(nextLayer);
		}
		
		return result;
	}
	shared Map<Planet,Integer> getLayeredPlanetsFromRoot(User perspective, Planet* rootPlanets) {
		MutableMap<Planet,Integer> tiers = HashMap<Planet,Integer>();
		void recur([Planet*] currentLayerPlanets, Integer layer) {
			if (currentLayerPlanets.empty) {
				return;
			}
			
			value nextLayer = ArrayList<Planet>();
			for (currentLayerPlanet in currentLayerPlanets) {
				if (!exists assignedTier = tiers.get(currentLayerPlanet)) {
					tiers.put(currentLayerPlanet, layer);
				}
				value connectedPlanets = wsw.getConnectedPlanets(currentLayerPlanet);
				for (connectedPlanet in connectedPlanets) {
					if (!tiers.keys.contains(connectedPlanet) && wsw.getPlanetState(connectedPlanet).owner==perspective) {
						nextLayer.add(connectedPlanet);
					}
				}
			}
			recur(nextLayer.sequence(), layer + 1);
		}
		recur(rootPlanets, 1);
		return tiers;
	}
	shared {PlanetState*} getOwnedPlanetsOrdererByProductivity(User perspective) {
		value allPlanets = wsw.getPlanets(ownedPlanetPredicate(perspective));
		return allPlanets.sort(byDecreasing(planetEvaluator.evaluate));
	}
	shared Boolean isBetter(Planet a, Planet b) {
		if (planetEvaluator.evaluate(wsw.getPlanetState(a)) >
			planetEvaluator.evaluate(wsw.getPlanetState(b))) {
			return true;
		} else {
			return false;
		}
	}
}
