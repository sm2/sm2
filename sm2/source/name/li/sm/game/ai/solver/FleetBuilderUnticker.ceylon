import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	ProportionalFleetSizeBuilder
}
import java.math {
	RoundingMode
}
import ceylon.math.decimal {
	round,
	Mode,
	halfUp
}
class FleetBuilderUnticker(ProportionalFleetSizeBuilder builder) satisfies PlanetUnticker {
	
	shared actual FunctionalPlanetState untick(FunctionalPlanetState previousState, FunctionalPlanetState nextState) {
		value states = [previousState, nextState];
		value incomingOwnerFleetSize = chooseGoodSlot(states*.incomingOwnerFleetSize);
		value incomingEnemyFleetSize = chooseGoodSlot(states*.incomingEnemyFleetSize);
		value owner = chooseGoodSlot(states*.owner);
		value existingEnemyFleetSize = nextState.enemyFleetSize;
		variable Slot<FleetSize> resultFriendlyFleetSize = BadSlot();
		
		if (exists nextExFrFs = nextState.ownerFleetSize.evaluate()) {
			value rawValue = nextExFrFs.integerValue.float / ((1 + builder.getRatio(nextState.planet)));
			value effectiveValue = if (rawValue.magnitude - rawValue.magnitude.integer >= 0.5) 
				then rawValue.integer + 1 
				else rawValue.integer;
			resultFriendlyFleetSize = ConstSlot(FleetSize(effectiveValue));
		}
		
		return FunctionalPlanetState(
			resultFriendlyFleetSize,
			existingEnemyFleetSize,
			incomingOwnerFleetSize,
			incomingEnemyFleetSize,
			owner,
			nextState.planet);
	}
	
}