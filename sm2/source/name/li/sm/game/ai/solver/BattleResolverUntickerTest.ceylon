import ceylon.test {
	test,
	assertEquals
}

import name.li.sm.model.fleets {
	FleetSize,
	Fleet
}
import name.li.sm.model.planets {
	PlanetState,
	Planet,
	BattleResolver
}
import name.li.sm.model.players {
	NormalPlayer
}
class BattleResolverUntickerTest() {
	
	value ticker = BattleResolver();
	value unticker = BattleResolverUnticker(ticker);
	
	value planet = Planet("foo", FleetSize(100));
	value owner = NormalPlayer("user");
	value enemy = NormalPlayer("enemy");
	value initialPs = PlanetState(owner, planet);
	
	test
	shared void testUntickKnownStateSameOwner() {
		
		value ownerFleetSize = FleetSize(100);
		value enemyFleetSize = FleetSize(75);
		value psPrev = initialPs.addFleet(Fleet(owner, ownerFleetSize)).addFleet(Fleet(enemy, enemyFleetSize));
		value psNext = ticker.tick(psPrev);
		value fpsPrev = newFunctionalPlanetStateFromPlanetState(psPrev);
		value fpsNext = newFunctionalPlanetStateFromPlanetState(psNext);
		
		value untickedFps = unticker.untick(fpsPrev, fpsNext);

		
		assertEquals(untickedFps.owner.evaluate(), owner);
		assertEquals(untickedFps.ownerFleetSize.evaluate(), ownerFleetSize);
		assertEquals(untickedFps.enemyFleetSize.evaluate(), enemyFleetSize);
	}
	
	test
	shared void testUntickKnownStateChangedOwner() {
		
		value ownerFleetSize = FleetSize(10);
		value enemyFleetSize = FleetSize(75);
		value psPrev = initialPs.addFleet(Fleet(owner, ownerFleetSize)).addFleet(Fleet(enemy, enemyFleetSize));
		value psNext = ticker.tick(psPrev);
		value fpsPrev = newFunctionalPlanetStateFromPlanetState(psPrev);
		value fpsNext = newFunctionalPlanetStateFromPlanetState(psNext);
		
		value untickedFps = unticker.untick(fpsPrev, fpsNext);

		
		assertEquals(untickedFps.owner.evaluate(), owner);
		assertEquals(untickedFps.ownerFleetSize.evaluate(), ownerFleetSize);
		assertEquals(untickedFps.enemyFleetSize.evaluate(), enemyFleetSize);
	}

	test
	shared void testPartialStateSameOwner() {
		
		value ownerFleetSize = FleetSize(100);
		value enemyFleetSize = FleetSize(75);
		value psPrev = initialPs.addFleet(Fleet(owner, ownerFleetSize)).addFleet(Fleet(enemy, enemyFleetSize));
		value psNext = ticker.tick(psPrev);
		value fpsPrev = newFunctionalPlanetStateFromPlanetState(psPrev);
		value fpsNext = newFunctionalPlanetStateFromPlanetState(psNext);
		
		value untickedFps = unticker.untick(fpsPrev.with{ ownerFleetSize = BadSlot(); }, fpsNext);
		
		assertEquals(untickedFps.owner.evaluate(), owner);
		assertEquals(untickedFps.ownerFleetSize.evaluate(), ownerFleetSize);
		assertEquals(untickedFps.enemyFleetSize.evaluate(), enemyFleetSize);

		value untickedFps2 = unticker.untick(fpsPrev.with{ enemyFleetSize = BadSlot(); }, fpsNext);
		
		assertEquals(untickedFps2.owner.evaluate(), owner);
		assertEquals(untickedFps2.ownerFleetSize.evaluate(), ownerFleetSize);
		assertEquals(untickedFps2.enemyFleetSize.evaluate(), enemyFleetSize);
	}
	
	test
	shared void testUntickPartialStateChangedOwner() {
		
		value ownerFleetSize = FleetSize(10);
		value enemyFleetSize = FleetSize(75);
		value psPrev = initialPs.addFleet(Fleet(owner, ownerFleetSize)).addFleet(Fleet(enemy, enemyFleetSize));
		value psNext = ticker.tick(psPrev);
		value fpsPrev = newFunctionalPlanetStateFromPlanetState(psPrev);
		value fpsNext = newFunctionalPlanetStateFromPlanetState(psNext);
		
		value untickedFps = unticker.untick(fpsPrev.with{ ownerFleetSize = BadSlot(); }, fpsNext);
		
		assertEquals(untickedFps.owner.evaluate(), owner);
		assertEquals(untickedFps.ownerFleetSize.evaluate(), ownerFleetSize);
		assertEquals(untickedFps.enemyFleetSize.evaluate(), enemyFleetSize);

		value untickedFps2 = unticker.untick(fpsPrev.with{ enemyFleetSize = BadSlot(); }, fpsNext);
		
		assertEquals(untickedFps2.owner.evaluate(), owner);
		assertEquals(untickedFps2.ownerFleetSize.evaluate(), ownerFleetSize);
		assertEquals(untickedFps2.enemyFleetSize.evaluate(), enemyFleetSize);
	}
}
