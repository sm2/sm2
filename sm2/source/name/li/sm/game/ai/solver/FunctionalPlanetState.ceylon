import name.li.sm.model.fleets {
	FleetSize,
	FleetsMap
}
import name.li.sm.model.planets {
	PlanetState,
	Planet
}
import name.li.sm.model.players {
	User
}

FunctionalPlanetState newFunctionalPlanetStateFromPlanetState(PlanetState ps) {
	return FunctionalPlanetState(
		FuncSlot(() => ps.ownerFleet.size),
		FuncSlot(() => getLargestEnemyFleetSize(ps.fleets, ps.owner)),
		FuncSlot(() => ps.incomingFleets.getFleet(ps.owner)?.size else FleetSize(0)),
		FuncSlot(() => getLargestEnemyFleetSize(ps.incomingFleets, ps.owner)),
		ConstSlot(ps.owner),
		ps.planet);
}

class FunctionalPlanetState(
	shared Slot<FleetSize> ownerFleetSize,
	shared Slot<FleetSize> enemyFleetSize,
	shared Slot<FleetSize> incomingOwnerFleetSize,
	shared Slot<FleetSize> incomingEnemyFleetSize,
	shared Slot<User> owner, 
	shared Planet planet) {
	
	shared FunctionalPlanetState with(Slot<FleetSize> ownerFleetSize = this.ownerFleetSize,
		Slot<FleetSize> enemyFleetSize = this.enemyFleetSize,
		Slot<FleetSize> incomingFriendlyFleetSize = this.incomingOwnerFleetSize,
		Slot<FleetSize> incomingEnemyFleetSize = this.incomingEnemyFleetSize,
		Slot<User> owner = this.owner) {
		return FunctionalPlanetState(ownerFleetSize, enemyFleetSize, incomingFriendlyFleetSize, incomingEnemyFleetSize, owner, planet);
	}
}

FleetSize getLargestEnemyFleetSize(FleetsMap fleets, User owner) {
	variable value resultFleetSize = FleetSize(0);
	value largestFleet = fleets.largestFleet;
	if (exists largestFleet) {
		if (largestFleet.owner == owner) {
			value secondLargestFleet = fleets.removeFleetForUser(largestFleet.owner).largestFleet;
			if (exists secondLargestFleet) {
				resultFleetSize = secondLargestFleet.size;
			}
		} else {
			resultFleetSize = largestFleet.size;
		}
	}
	return resultFleetSize;
}
