class BadSlot() satisfies Slot<Nothing> {
	shared actual Null evaluate() => null;
}