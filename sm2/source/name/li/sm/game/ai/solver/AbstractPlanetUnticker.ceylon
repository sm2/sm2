import name.li.sm.model.planets {
	PlanetTicker
}
abstract class AbstractUnticker<ConcretePlanetTickerClass>(shared ConcretePlanetTickerClass ticker) satisfies PlanetUnticker
		given ConcretePlanetTickerClass satisfies PlanetTicker {
}
