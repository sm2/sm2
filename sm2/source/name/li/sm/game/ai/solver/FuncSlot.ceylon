class FuncSlot<Type>(Type() func) satisfies Slot<Type> {
	shared actual Type evaluate() => func();
	
	shared actual String string => "Slot: { `` this.evaluate()?.string else "null" ``}";
}