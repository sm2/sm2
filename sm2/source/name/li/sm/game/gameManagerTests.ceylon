import ceylon.test {
	test,
	assertNotEquals
}
import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.world {
	WorldStateBuilder,
	WorldBuilder,
	WorldStateWrapper
}
import name.li.sm.model.players {
	neutralPlayer,
	NormalPlayer
}
import name.li.sm.model.planets {
	BattleResolver,
	ProportionalFleetSizeBuilder,
	FleetMovementsResolver,
	Planet,
	LoggerTicker
}
import name.li.sm.model.api {
	SimpleMovesAdapter
}
import name.li.sm.logging {
	initLogging
}
import name.li.sm.game.ai.simple {
	SimpleAi
}

class GameManagerTests() {
	
	value auir = Planet("Auir", FleetSize(1000));
	value terra = Planet("Terra", FleetSize(1000));
	value zerus = Planet("Zerus", FleetSize(1000));
	value char = Planet("Char", FleetSize(500));
	
	value zerg = NormalPlayer("Zerg");
	value terran = NormalPlayer("Terran");
	value protoss = NormalPlayer("Protoss");
	
	value standardTicker = LoggerTicker({FleetMovementsResolver()}, true).chain(BattleResolver()).chain(ProportionalFleetSizeBuilder(0.1));
	
	value world = WorldBuilder(standardTicker)
			.withPlayers(zerg, terran, protoss)
			.withPlanet(char).connectedTo(zerus, terra)
			.withPlanet(auir).connectedTo(terra)
			.withPlanet(zerus).connectedTo(terra)
			.build();
	
	value initialWorldState = WorldStateBuilder(world, neutralPlayer)
			.withPlayer(protoss).havingFleet(auir, FleetSize(100))
			.withPlayer(zerg).havingFleet(zerus, FleetSize(1000))
			.withPlayer(zerg).havingFleet(char, FleetSize(1000))
			.build();
	
	
	test
	shared void testTicking() {
		initLogging();
		value gm = GameManager(initialWorldState, [zerg, terran, protoss].map((p) => SimpleMovesAdapter(world, p, (ws) => SimpleAi(p).getMoves(ws))));
		value stateBeforeTick = WorldStateWrapper(gm.currentWorldState).getPlanetState(auir);
		gm.tick();
		value stateAfterTick = WorldStateWrapper(gm.currentWorldState).getPlanetState(auir);
		assertNotEquals(stateBeforeTick.ownerFleet, stateAfterTick.ownerFleet);
	}
	
}
