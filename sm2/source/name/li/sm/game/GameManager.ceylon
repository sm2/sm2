import ceylon.promise {
	Deferred,
	Promise
}
import name.li.sm.logging {
	LoggingSupport
}
import name.li.sm.model.api {
	MovesSupplier
}
import name.li.sm.model.fleets {
	Fleet
}
import name.li.sm.model.world {
	WorldState,
	Move,
	WorldStateWrapper
}
import java.util.concurrent {
	CountDownLatch,
	CompletableFuture
}
import ceylon.language.meta.model {
	Function
}

class GameManager(WorldState initialWorldState, {MovesSupplier*} ms) satisfies LoggingSupport {
	
	shared variable WorldState currentWorldState = initialWorldState;
	
	shared void tick() {
		value promise = getAllMovesPromise(ms);
		native("jvm") value fut = CompletableFuture<{Move*}>();
		native("jvm") value oncomplete = void ({Move*}|Throwable completion) {
			if(is {Move*} completion) {
				fut.complete(completion);
			} else {
				throw Exception("Failed to get moves");
			}
		};
		promise.onComplete(oncomplete);
		native("jvm") value moves = fut.get();
		applyMoves(moves);
		currentWorldState = currentWorldState.tick();
		logger("Tick completed");
		logger("Current state: ``currentWorldState``");
	}
	
	Promise<{Move*}> getAllMovesPromise({MovesSupplier*} ms) {
		value blankDeferred = Deferred<{Move*}>();
		blankDeferred.fulfill([]);
		return ms*.getMoves(currentWorldState).fold(blankDeferred.promise)((acc, e) {
				return e.map((moves) {
						logger("Got moves: ``moves``");
						return moves;
					}).and(acc).map((a, b) => a.chain(b));
			});
	}
	
	void applyMoves({Move*} moves) {
		for (move in moves) {
			currentWorldState = applyMove(move);
		}
	}
	
	WorldState applyMove(Move move) {
		logger("Applying move ``move``");
		variable value worldState = currentWorldState; 
		try {
			value wsw = WorldStateWrapper(currentWorldState);
			value from = wsw.getPlanetState(move.from);
			value to = wsw.getPlanetState(move.to);
			value fleet = Fleet(move.player, move.size);
			value newFrom = from.departFleet(fleet);
			value newTo = to.addIncomingFleet(fleet);
			assert (exists newFrom);
			worldState = currentWorldState.updatePlanetStates({ newTo, newFrom });
		} catch (Exception e) {
			logger("Failed to apply move: ``move``");
		}
		logger("Move applied ``move``");
		return worldState;
	}
}
