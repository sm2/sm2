module name.li.sm.game "0.0.1" {
	shared import name.li.sm.model "0.0.1";
	import ceylon.collection "1.2.1";
	import ceylon.logging "1.2.1";
	import ceylon.test "1.2.1";
	native("jvm") import ceylon.math "1.2.1";
	native("jvm") import java.base "8";
}
