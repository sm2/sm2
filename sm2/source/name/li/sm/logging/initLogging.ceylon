import ceylon.language.meta.declaration {
	Module,
	Package
}
import ceylon.logging {
	addLogWriter,
	Priority,
	info
}
shared void initLogging() {
	addLogWriter {
		void log(Priority p, Module|Package c, String m, Throwable? e) {
			value print = p <= info
					then process.writeLine
					else process.writeError;
			print("[``system.milliseconds``] ``p.string`` ``m``");
			if (exists e) {
				printStackTrace(e, print);
			}
		}
	};
}
