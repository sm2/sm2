import ceylon.logging {
	getCeylonLogger=logger
}

shared interface LoggingSupport {
	
	shared default void logger(String message) => getCeylonLogger(`module name.li.sm.logging`).info(message);
}
