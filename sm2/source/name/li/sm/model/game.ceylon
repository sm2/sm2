import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	BattleResolver,
	FleetMovementsResolver,
	Planet,
	ProportionalFleetSizeBuilder
}
import name.li.sm.model.players {
	neutralPlayer,
	NormalPlayer
}
import name.li.sm.model.world {
	WorldBuilder,
	WorldStateBuilder
}

shared void game() {
	
	value auir = Planet("Auir", FleetSize(1000));
	value terra = Planet("Terra", FleetSize(1000));
	value zerus = Planet("Zerus", FleetSize(1000));
	value char = Planet("Char", FleetSize(500));
	
	value zerg = NormalPlayer("Zerg");
	value terran = NormalPlayer("Terran");
	value protoss = NormalPlayer("Protoss");
	
	value standardTicker = FleetMovementsResolver().chain(BattleResolver()).chain(ProportionalFleetSizeBuilder(0.1));
	
	value world = WorldBuilder(standardTicker)
		.withPlayers(zerg, terran, protoss)
		.withPlanet(char).connectedTo(zerus, terra)
		.withPlanet(auir).connectedTo(terra)
		.withPlanet(zerus).connectedTo(terra)
		.build();
	
	value initialWorldState = WorldStateBuilder(world, neutralPlayer)
		.withPlayer(protoss).havingFleet(auir, FleetSize(100))
		.withPlayer(zerg).havingFleet(zerus, FleetSize(500))
		.build();
	
	print(initialWorldState);
}
