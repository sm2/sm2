import ceylon.test {
	test,
	assertEquals
}


import name.li.sm.model.fleets {
	Fleet,
	FleetSize,
	FleetsMap
}
import name.li.sm.model.players {
	NormalPlayer
}

class PlanetsTests() {
	value maxFleetSize = FleetSize(1000);
	value auir = Planet("Auir", maxFleetSize);
	value protoss = NormalPlayer("protoss");
	value zerg = NormalPlayer("zerg");
	value terran = NormalPlayer("terran");
	value tickerRatio = 0.2;
	
	test
	shared void testProportionalFleetSizeBuilderNormal() {
		value initialFleetSizeInt = 100;
		value state = PlanetState(protoss, auir, FleetsMap().addFleet(Fleet(protoss, FleetSize(initialFleetSizeInt))));
		value ticker = ProportionalFleetSizeBuilder(tickerRatio);
		value newState = ticker.tick(state);
		assertEquals(newState.ownerFleet.size, FleetSize((initialFleetSizeInt * (1 + tickerRatio)).integer));
	}
	
	test
	shared void testProportionalFleetSizeBuilderCapped() {
		value initialFleetSizeInt = 990;
		value state = PlanetState(protoss, auir, FleetsMap().addFleet(Fleet(protoss, FleetSize(initialFleetSizeInt))));
		value ticker = ProportionalFleetSizeBuilder(tickerRatio);
		value newState = ticker.tick(state);
		assertEquals(newState.ownerFleet.size, FleetSize(1000));
	}

	test
	shared void testBattleTickerSimple() {
		value initialFleetSizeInt = 100;
		variable value state = PlanetState(protoss, auir, FleetsMap().addFleet(Fleet(protoss, FleetSize(initialFleetSizeInt))));
		value enemySizeInt = 80;
		state = state.addFleet(Fleet(zerg, FleetSize(enemySizeInt)));
		value ticker = BattleResolver();
		value newState = ticker.tick(state);

		assertEquals(newState.ownerFleet.size, FleetSize(initialFleetSizeInt - enemySizeInt));
		assertEquals(newState.ownerFleet.owner, protoss);
	}
	
	test
	shared void testBattleTickerComplex() {
		value initialFleetSizeInt = 100;
		variable value state = PlanetState(protoss, auir, FleetsMap().addFleet(Fleet(protoss, FleetSize(initialFleetSizeInt))));
		value enemySizeInt1 = 110;
		value enemySizeInt2 = 130;
		state = state.addFleet(Fleet(zerg, FleetSize(enemySizeInt1)));
		state = state.addFleet(Fleet(terran, FleetSize(enemySizeInt2)));
		value ticker = BattleResolver();
		value newState = ticker.tick(state);
		
		assertEquals(newState.ownerFleet.size, FleetSize(20));
		assertEquals(newState.ownerFleet.owner, terran);
	}
	
	test
	shared void testPlanetTickerChaining() {
		value initialFleetSizeInt = 100;
		variable value state = PlanetState(protoss, auir, FleetsMap().addFleet(Fleet(protoss, FleetSize(initialFleetSizeInt))));
		value enemyFleetSizeInt = 110;
		state = state.addFleet(Fleet(zerg, FleetSize(enemyFleetSizeInt)));
		value ticker1 = BattleResolver().chain(ProportionalFleetSizeBuilder(0.2));
		value newState1 = ticker1.tick(state);
		
		assertEquals(newState1.ownerFleet.size, FleetSize(12));
		assertEquals(newState1.ownerFleet.owner, zerg);

		value ticker2 = ProportionalFleetSizeBuilder(0.2).chain(BattleResolver());
		value newState2 = ticker2.tick(state);

		assertEquals(newState2.ownerFleet.size, FleetSize(10));
		assertEquals(newState2.ownerFleet.owner, protoss);
		
	}
}
