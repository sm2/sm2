shared class Path(shared Planet planet, shared Planet otherPlanet) {
	shared actual String string => "Path { ``planet.name`` <-> ``otherPlanet.name`` }";
	shared actual Boolean equals(Object that) {
		if (is Path that) {
			return planet == that.planet &&
					otherPlanet == that.otherPlanet;
		} else {
			return false;
		}
	}
}
