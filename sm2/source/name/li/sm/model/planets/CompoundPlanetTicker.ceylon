shared class CompoundPlanetTicker(shared PlanetTicker* tickers) satisfies PlanetTicker {

	shared actual PlanetState tick(PlanetState planetState) {
		return tickers.fold(planetState)((acc, e) => e.tick(acc));
	}
	
	shared actual CompoundPlanetTicker chain(PlanetTicker ticker) {
		return CompoundPlanetTicker(*tickers.withTrailing(ticker));
	}
	
}