import name.li.sm.model.fleets {
	FleetsMap
}
shared class FleetMovementsResolver() satisfies PlanetTicker {
	shared actual PlanetState tick(PlanetState planetState) {
		value newFleetsMap = planetState.fleets.add(planetState.incomingFleets);
		return planetState.forkWith {
			incomingFleets = FleetsMap();
			outgoingFleets = FleetsMap();
			fleets = newFleetsMap;
		};
	}
}
