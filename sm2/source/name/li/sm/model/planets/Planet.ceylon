import name.li.sm.model.fleets {
	FleetSize
}
shared class Planet(shared String name, shared FleetSize maxFleetSize) {
	shared actual String string => "Planet ``name``, maxsize: ``maxFleetSize``";
	
	shared actual Boolean equals(Object that) {
		if (is Planet that) {
			return name == that.name &&
					maxFleetSize == that.maxFleetSize;
		} else {
			return false;
		}
	}
}
