import ceylon.logging {
	getCeylonLogger=logger
}

import name.li.sm.model.fleets {
	FleetSize
}
shared class ProportionalFleetSizeBuilder(Float|Float(Planet) proportionalRatio) satisfies CappedFleetSizeBuilder {
	
	shared actual FleetSize calcGrowth(PlanetState p) {
		return FleetSize((p.ownerFleet.size.integerValue * getRatio(p.planet)).integer);
	}
	
	shared Float getRatio(Planet p) {
		switch (proportionalRatio)
		case (is Float) {
			return proportionalRatio;
		}
		case (is Float(Planet)) {
			return proportionalRatio(p);
		}
	}

	shared actual void logger(String message) =>  getCeylonLogger(`module name.li.sm.model`).info(message);
	
}