import name.li.sm.model.fleets {
	Fleet,
	FleetSize
}
import name.li.sm.logging {
	LoggingSupport
}
shared interface CappedFleetSizeBuilder satisfies PlanetTicker & LoggingSupport {
	
	shared actual PlanetState tick(PlanetState planetState) {
		value possibleGrowth = calcGrowth(planetState);
		value freeSpaceOnPlanet = freeSpace(planetState, planetState.planet.maxFleetSize);
		if (exists freeSpaceOnPlanet) {
			variable value growth = possibleGrowth;
			if (growth > freeSpaceOnPlanet) {
				growth = freeSpaceOnPlanet;
				value lostShips = possibleGrowth.subtract(freeSpaceOnPlanet);
				assert(exists lostShips);
				logger("Losing ``lostShips.integerValue`` ships due to planet size limit on ``planetState.planet.name``");
			}
			return planetState.addFleet(Fleet(planetState.owner, growth));
		} else {
			return planetState.fork();
		}
	}
	
	FleetSize? freeSpace(PlanetState planetState, FleetSize maxFleetSize) {
		value currentOwnerFleetSize = planetState.ownerFleet.size;
		if (currentOwnerFleetSize >= maxFleetSize) {
			return null;
		} else {
			value possiblerowth = maxFleetSize.subtract(currentOwnerFleetSize);
			assert (exists possiblerowth);
			return possiblerowth;
		}
	}
	
	shared formal FleetSize calcGrowth(PlanetState p);
}
