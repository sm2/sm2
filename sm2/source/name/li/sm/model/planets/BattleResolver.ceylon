import name.li.sm.model.fleets {
	Fleet,
	FleetSize,
	FleetsMap
}
shared class BattleResolver() satisfies PlanetTicker {
	
	shared actual PlanetState tick(PlanetState planetState) {
		value largestFleet = planetState.fleets.largestFleet;
		if (exists largestFleet) {
			value secondLargestFleet = planetState.fleets.removeFleetForUser(largestFleet.owner).largestFleet;
			if (exists secondLargestFleet) {
				if (secondLargestFleet.size == largestFleet.size) {
					return forkPlanetState(planetState, Fleet(planetState.owner, FleetSize(0)));
				} else {
					value winnerFleetSize = largestFleet.size.subtract(secondLargestFleet.size);
					assert (exists winnerFleetSize);
					return forkPlanetState(planetState, Fleet(largestFleet.owner, winnerFleetSize));
				}
			} else {
				return forkPlanetState(planetState, largestFleet);
			}
		} else {
			return planetState.fork();
		}
	}
	
	PlanetState forkPlanetState(PlanetState planetState, Fleet winningFleet) {
		return planetState.forkWith {
			owner = winningFleet.owner;
			fleets = FleetsMap().addFleet(winningFleet);
			incomingFleets = FleetsMap();
			outgoingFleets = FleetsMap();
		};
	}
}
