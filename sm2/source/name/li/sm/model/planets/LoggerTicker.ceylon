import name.li.sm.logging {
	LoggingSupport
}
shared class LoggerTicker({PlanetTicker*} tickers, Boolean supportChaining = false) satisfies PlanetTicker & LoggingSupport {

	shared actual PlanetState tick(PlanetState initialPlanetState) {
		variable value planetState = initialPlanetState;
		for (ticker in tickers) {
			logger("Ticker: ``className(ticker)``");
			logger("Before tick: ``planetState``");
			planetState = ticker.tick(planetState);
			logger("After tick: ``planetState``");
		}
		return planetState;
	}
	
	// XXX: chainig should be transparent
	shared PlanetTicker chainLog(PlanetTicker nextTicker) {
		return supportChaining then LoggerTicker(tickers.chain({nextTicker}), true) else CompoundPlanetTicker(nextTicker);
	}
}
