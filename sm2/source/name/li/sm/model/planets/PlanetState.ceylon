import ceylon.collection {
	HashMap
}

import name.li.sm.model.fleets {
	Fleet,
	FleetsMap,
	FleetSize
}
import name.li.sm.model.players {
	User
}

PlanetState newPlanetState(User owner, Planet planet, Fleet* fleets) {
	value fleetsMap = fleets.fold(HashMap<User,Fleet>())((acc, e) {
			acc.put(e.owner, e);
			return acc;
		});
	
	return PlanetState(owner, planet, FleetsMap(fleetsMap));
}

shared Planet planetStateToPlanet(PlanetState ps) {
	return ps.planet;
}

shared class PlanetState(owner, planet, variable FleetsMap initialFleets = FleetsMap(), incomingFleets = FleetsMap(), outgoingFleets = FleetsMap()) {
	
	shared Planet planet;
	shared User owner;
	shared FleetsMap fleets = initialFleets;
	shared FleetsMap incomingFleets;
	shared FleetsMap outgoingFleets;
	
	if (initialFleets.getFleet(owner) is Null) {
		initialFleets = initialFleets.addFleet(Fleet(owner, FleetSize(0)));
	}
	
	shared PlanetState addFleet(Fleet fleet) {
		return PlanetState(owner, planet, fleets.addFleet(fleet));
	}
	
	shared PlanetState removeFleetForUser(User user) {
		return forkWith{ fleets = fleets.removeFleetForUser(user); };
	}
	
	shared PlanetState? removeFleet(Fleet fleet) {
		if (exists newFleetMap = fleets.removeFleet(fleet)) {
			return forkWith { fleets = newFleetMap; };
		} else {
			return null; // XXX: loks stupid
		}
	}
	
	shared PlanetState? departFleet(Fleet fleet) {
		if(exists newFleetMap = fleets.removeFleet(fleet)) {
			return forkWith {
				fleets = newFleetMap;
				outgoingFleets = outgoingFleets.addFleet(fleet);
			};
		} else {
			return null; // XXX: loks stupid
		}
	}
	
	shared PlanetState addIncomingFleet(Fleet fleet) {
		return forkWith { incomingFleets = incomingFleets.addFleet(fleet); };
	}
	
	shared PlanetState addOutgoingFleet(Fleet fleet) {
		return forkWith { outgoingFleets = outgoingFleets.addFleet(fleet); };
	}
	
	shared PlanetState changeOwner(User newOwner) {
		return PlanetState(newOwner, planet, fleets);
	}
	
	shared PlanetState forkWith(
		User owner = this.owner,
		FleetsMap fleets = this.fleets,
		FleetsMap incomingFleets = this.incomingFleets,
		FleetsMap outgoingFleets = this.outgoingFleets) {
		return PlanetState(owner, planet, fleets, incomingFleets, outgoingFleets);
	}
	
	shared actual String string => "``planet.name`` { fleets: ``fleets``, inc: ``incomingFleets``, out: ``outgoingFleets`` }";
	
	// convenience methods
	
	shared Fleet ownerFleet {
		value ownerFleet = fleets.getFleet(owner);
		assert (exists ownerFleet);
		return ownerFleet;
	}
	
	shared PlanetState fork() {
		return forkWith();
	}
}
