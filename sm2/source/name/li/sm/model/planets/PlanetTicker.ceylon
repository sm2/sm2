shared interface PlanetTicker {

	shared formal PlanetState tick(PlanetState planetState);
	
	
	shared default CompoundPlanetTicker chain(PlanetTicker ticker) {
		return CompoundPlanetTicker(*[this, ticker]);
	}
	
	
}