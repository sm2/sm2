import ceylon.collection {
	HashSet
}
import ceylon.test {
	test,
	assertEquals,
	assertTrue,
	assertFalse
}

import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	Planet,
	PlanetState
}
import name.li.sm.model.players {
	neutralPlayer,
	NormalPlayer
}


class WorldTests() {
	variable Planet auir = Planet("Auir", FleetSize(1000));
	variable Planet terra = Planet("Terra", FleetSize(1000));
	variable Planet moon = Planet("Moon", FleetSize(1));
	variable Planet mars = Planet("Mars", FleetSize(1));
	variable Planet zerus = Planet("Zerus", FleetSize(1000));
	variable Planet char = Planet("Char", FleetSize(500));
	variable NormalPlayer zerg = NormalPlayer("Zerg");
	variable NormalPlayer terran = NormalPlayer("Terran");
	variable NormalPlayer protoss = NormalPlayer("Protoss");
	variable World world = WorldBuilder()
			.withPlanet(auir).connectedTo(zerus)
			.withPlanet(terra).connectedTo(zerus)
			.withPlanet(char).connectedTo(zerus, terra)
			.withPlanet(moon).connectedTo(terra)
			.withPlanet(mars).connectedTo(terra, zerus)
			.withPlayers(protoss, zerg, terran)
			.build();
	variable WorldState initialWorldState = WorldStateBuilder(world, neutralPlayer)
			.withPlayer(protoss).havingFleet(auir, FleetSize(100))
			.withPlayer(terran).havingFleet(moon, FleetSize(1))
			.withPlayer(terran).havingFleet(mars, FleetSize(1))
			.withPlayer(zerg).havingFleet(zerus, FleetSize(500))
			.withPlayer(terran).havingFleet(terra, FleetSize(250))
			.build();

	value assertSamePlanets = ({Planet*} ps1, {Planet*} ps2) => assertEquals(HashSet{ *ps1 }, HashSet{ *ps2 });
	
	test
	shared void testFleetBuilderConnection() {
		value ww = WorldWrapper(world);
		assertSamePlanets(ww.getConnectedPlanets(zerus), [auir, terra, char, mars]);
		assertSamePlanets(ww.getConnectedPlanets(auir), [zerus]);
		assertTrue(ww.areConnected(auir, zerus));
		assertFalse(ww.areConnected(auir, char));
	}
	
	
	test
	shared void testWorldWrapperPredicates() {
		value wsw = WorldStateWrapper(initialWorldState);
		value planets = ({PlanetState*} planetStates) => planetStates.map((ps) => ps.planet);
		
		assertSamePlanets(planets(wsw.getPlanets(borderPlanetsPredicate(terran))), [mars, terra]);
		assertSamePlanets(planets(wsw.getPlanets(borderPlanetsPredicate(protoss))), [auir]);

		assertSamePlanets(planets(wsw.getPlanets(ownedPlanetPredicate(zerg))), [zerus]);

		assertSamePlanets(planets(wsw.getPlanets(notOwnedPlanetPredicate(protoss))), [zerus, terra, char, moon, mars]);

		assertSamePlanets(planets(wsw.getPlanets(neutralPlanetPredicate)), [char]);
	}
}
