import name.li.sm.model.players {
	NormalPlayer,
	neutralPlayer
}
import name.li.sm.model.planets {
	Planet
}
import name.li.sm.model.fleets {
	FleetSize
}
import ceylon.test {
	assertTrue,
	assertEquals,
	test
}
class WorldStateTest() {
	
	variable Planet auir = Planet("Auir", FleetSize(1000));
	variable Planet terra = Planet("Terra", FleetSize(1000));
	variable NormalPlayer terran = NormalPlayer("Terran");
	variable NormalPlayer protoss = NormalPlayer("Protoss");
	variable World world = WorldBuilder()
		.withPlanet(auir).connectedTo(terra)
		.withPlayers(protoss, terran)
		.build();
	variable WorldState initialWorldState = WorldStateBuilder(world, neutralPlayer)
		.withPlayer(protoss).havingFleet(auir, FleetSize(100))
		.withPlayer(terran).havingFleet(terra, FleetSize(250))
		.build();
	
	test
	shared void testUpdatePlanetStates() {
		
		value wsw = WorldStateWrapper(initialWorldState);
		value initialAuirPs = wsw.getPlanetState(auir);
		value initialTerraPs = wsw.getPlanetState(terra);
		
		value updatedAuirPs = wsw.getPlanetState(auir).fork();
		value updatedWs = initialWorldState.updatePlanetStates({ updatedAuirPs });
		value updatedPsList = updatedWs.planetStates;

		assertEquals(updatedPsList.size, 2);
		assertTrue(updatedPsList.contains(initialTerraPs));
		assertTrue(updatedPsList.contains(updatedAuirPs));
		assertTrue(!updatedPsList.contains(initialAuirPs));
		
	}
}
