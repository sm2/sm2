import ceylon.collection {
	HashSet
}

import name.li.sm.model.planets {
	Planet,
	PlanetTicker
}
import name.li.sm.model.players {
	User
}
shared class World(
	shared {Planet*} planets,
	shared {User*} players,
	shared Map<Planet,HashSet<Planet>> topology,
	shared PlanetTicker ticker) {
}
