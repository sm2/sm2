import name.li.sm.model.planets {
	Planet
}
shared class WorldWrapper(World w) {
	
	shared {Planet*} getConnectedPlanets(Planet planet) {
		if (exists value connectedPlanets = w.topology.get(planet)) {
			return connectedPlanets.sequence();
		} else {
			return {};
		}
	}
	
	shared Boolean areConnected(Planet a, Planet b) {
		return getConnectedPlanets(a).contains(b);
	}
}