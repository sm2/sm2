import name.li.sm.model.fleets {
	FleetSize
}
import name.li.sm.model.planets {
	Planet
}
import name.li.sm.model.players {
	User
}
shared class Move(shared User player, shared Planet from, shared Planet to, shared FleetSize size) {
	shared actual String string => "Move: { by: ``player.name``, from: ``from.name``, to ``to.name``, size: ``size.integerValue`` }";
}
