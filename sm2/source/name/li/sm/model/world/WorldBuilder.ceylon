import ceylon.collection {
	HashMap,
	ArrayList,
	HashSet
}
import name.li.sm.model.planets {
	Planet,
	PlanetTicker,
	ProportionalFleetSizeBuilder
}
import name.li.sm.model.players {
	User
}

shared class WorldBuilder(PlanetTicker ticker = ProportionalFleetSizeBuilder(0.1)) {
	value planets = HashSet<Planet>();
	value connectedPlanets = HashMap<Planet,HashSet<Planet>>();
	value users = ArrayList<User>();
	
	void addConnection(Planet from, Planet to) {
		planets.add(from);
		planets.add(to);
		value soFarConnected = connectedPlanets.get(from) else HashSet<Planet>();
		soFarConnected.add(to);
		connectedPlanets.put(from, soFarConnected);
	}
	
	void addBidirectionalConnection(Planet planet1, Planet planet2) {
		addConnection(planet1, planet2);
		addConnection(planet2, planet1);
	}
	
	shared class PlanetBuilder(Planet planet) {
		shared WorldBuilder connectedTo(Planet* planets) {
			for (planetToConnectTo in planets) {
				addBidirectionalConnection(planet, planetToConnectTo);
			}
			return outer;
		}
	}
	
	shared PlanetBuilder withPlanet(Planet planet) {
		return PlanetBuilder(planet);
	}
	
	shared WorldBuilder withPlayers(User+ players) {
		users.addAll(players);
		return this;
	}
	
	shared World build() {
		return World({ for (p in planets) p }, { for (u in users) u }, connectedPlanets, ticker);
	}
}
