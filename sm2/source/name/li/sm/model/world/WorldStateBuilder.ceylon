import ceylon.collection {
	HashMap
}
import name.li.sm.model.fleets {
	Fleet,
	FleetSize
}
import name.li.sm.model.planets {
	Planet,
	PlanetState
}
import name.li.sm.model.players {
	User
}

shared class WorldStateBuilder(World world, User defaultOwner) {
	value planetstates = HashMap<Planet,PlanetState>();
	
	for (planet in world.planets) {
		planetstates.put(planet, PlanetState(defaultOwner, planet));
	}
	
	shared class UserFleetOnPlanetBuilder(User player) {
		assert (player in world.players);
		shared WorldStateBuilder havingFleet(Planet planet, FleetSize size) {
			value ps = planetstates.get(planet);
			assert (exists ps);
			planetstates.put(planet, ps.changeOwner(player).addFleet(Fleet(player, size)));
			return outer;
			// XXX: what if two fleets are on the same planet?
		}
	}
	
	shared UserFleetOnPlanetBuilder withPlayer(User player) {
		return UserFleetOnPlanetBuilder(player);
	}
	
	shared WorldState build() {
		return WorldState(world, planetstates.items);
	}
}
