import name.li.sm.model.planets {
	PlanetState
}

shared class WorldState(shared World world, shared {PlanetState*} planetStates) {

	shared WorldState tick() {
		return WorldState(world, planetStates.map((PlanetState element) => world.ticker.tick(element)).sequence());
	}
	
	shared WorldState updatePlanetStates({PlanetState*} updatedPlanetStates) {
		return WorldState(world, planetStates.select((ps) => !updatedPlanetStates*.planet.contains(ps.planet)).chain(updatedPlanetStates));
	}
}
