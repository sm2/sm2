import ceylon.collection {
	HashSet
}

import name.li.sm.model.planets {
	PlanetState,
	Planet
}
import name.li.sm.model.players {
	NeutralPlayer,
	User
}
Predicate not(Predicate p) {
	return Predicate((ps, ws) => !p.func(ps, ws));
}
Predicate and(Predicate p1, Predicate p2) {
	return Predicate((ps, ws) => p1.apply(ps, ws) && p2.apply(ps, ws));
}
shared class Predicate(shared Boolean(PlanetState, WorldStateWrapper) func) {
	shared Boolean apply(PlanetState ps, WorldStateWrapper ws) {
		return func(ps, ws);
	}
}
shared Predicate notOwnedPlanetPredicate(User owner) {
	return Predicate(not(ownedPlanetPredicate(owner)).func);
}
shared Predicate ownedPlanetPredicate(User owner) {
	return Predicate((ps, ws) => ps.owner == owner);
}
shared Predicate neutralPlanetPredicate = Predicate((ps, ws) => ps.owner is NeutralPlayer);


shared Predicate borderPlanetsPredicate(User prespective) {
	return Predicate((ps, ws) { // XXX: should curry
		return ownedPlanetPredicate(prespective).apply(ps, ws) 
				&& ws.connectedPlanets(ps.planet).any((PlanetState cp) => cp.owner != prespective);
	});
}
	
shared class WorldStateWrapper(shared WorldState worldState) extends WorldWrapper(worldState.world) {
	
	shared {PlanetState*} getPlanets(Predicate p) {
		return worldState.planetStates.filter((ps) => p.apply(ps, this));
	}
	
	shared PlanetState getPlanetState(Planet planet) {
		value result = worldState.planetStates.find((PlanetState element) => element.planet == planet);
		"Failed to find planet state for planet`"
		assert(exists result);
		return result;
	}
	
	shared {PlanetState*} connectedPlanets(Planet planet) {
		value connectedPlanets = (worldState.world.topology.get(planet) else HashSet());
		return getPlanets(Predicate((ps, ws) => ps.planet in connectedPlanets));
	}
}