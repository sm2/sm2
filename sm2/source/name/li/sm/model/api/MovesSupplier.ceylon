import ceylon.promise {
	Promise
}

import name.li.sm.model.world {
	WorldState,
	Move
}
import name.li.sm.model.players {
	User
}
shared interface MovesSupplier {
	shared formal Promise<{Move*}> getMoves(WorldState ws);
	shared formal User player;
}