import name.li.sm.model.players {
	User
}
import name.li.sm.model.world {
	Move,
	WorldState,
	World
}
import ceylon.promise {
	Promise,
	Deferred
}

shared class SimpleMovesAdapter(World world, shared actual User player, {Move*}(WorldState) func) satisfies MovesSupplier {
	assert (player in world.players);

	shared actual Promise<{Move*}> getMoves(WorldState worldState) {
		value moves = func(worldState);
		for (move in moves) {
			"'From' planet does not exist"
			assert (move.from in world.planets);
			"'To' planet does not exist"
			assert (move.to in world.planets);	
			"Correct player"
			assert (player == move.player);
		}
		value def = Deferred<{Move*}>();
		def.fulfill(moves);
		return def.promise;
	}
	
}