import ceylon.test {
	test,
	assertThatException
}

class FleetSizeMathTest() {
	
	value zero = FleetSize(0);
	value one = FleetSize(1);
	value two = FleetSize(2);
	value three = FleetSize(3);
	
	test
	shared void testBbadSubtracting() {
		assert(!zero.subtract(one) exists);
	}
	
	test
	shared void testNegatives() {
		assertThatException(() => FleetSize(-1));
	}
	
	test
	shared void testAdding() {
		assert(one.add(two) == three);
	}
	
	test
	shared void testGoodSubtracting() {
		value calc = three.subtract(two);
		assert(exists calc);
		assert(calc == one);
	}
}
