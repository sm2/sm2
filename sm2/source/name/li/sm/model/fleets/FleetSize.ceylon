shared class FleetSize(integerValue) satisfies Comparable<FleetSize> {
	shared Integer integerValue;
	
	shared FleetSize add(FleetSize other) {
		return FleetSize(integerValue + other.integerValue);
	}
	shared FleetSize? subtract(FleetSize other) {
		if (other.largerThan(this)) {
			return null;
		} else {
			return FleetSize(integerValue - other.integerValue);
		}
	}
	shared actual Comparison compare(FleetSize other) {
		if (other.integerValue > integerValue) {
			return smaller;
		} else if (other.integerValue == integerValue) {
			return equal;
		} else {
			return larger;
		}
	}
	
	shared actual Boolean equals(Object that) {
		if (is FleetSize that) {
			return integerValue == that.integerValue;
		} else {
			return false;
		}
	}
	
	shared actual String string => "FleetSize(``integerValue``)";
}