
import name.li.sm.model.players {
	User
}
shared class Fleet(owner, size) {
	shared User owner;
	shared FleetSize size;
	
	shared actual String string => "Fleet { user = ``owner.name``, size: ``size.integerValue``";
}