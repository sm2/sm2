import ceylon.collection {
	HashMap
}
import name.li.sm.model.players {
	User
}

shared class FleetsMap(Map<User,Fleet> fleetsMap = HashMap<User,Fleet>()) {
	value fleets = fleetsMap;
	
	shared FleetsMap addFleet(Fleet fleet) {
		value newFleetsMap = HashMap { *fleets.coalesced };
		variable value targetPlayerFleetSize = FleetSize(0);
		if (exists existingUserFleet = getFleet(fleet.owner)) {
			targetPlayerFleetSize = existingUserFleet.size;
		}
		newFleetsMap.put(fleet.owner, Fleet(fleet.owner, targetPlayerFleetSize.add(fleet.size)));
		return FleetsMap(newFleetsMap);
	}
	
	shared FleetsMap removeFleetForUser(User user) {
		return FleetsMap(fleets.clone().filterKeys((User key) => key != user));
	}
	
	shared FleetsMap? removeFleet(Fleet fleet) {
		variable value targetPlayerFleetSize = FleetSize(0);
		if (exists existingUserFleet = getFleet(fleet.owner)) {
			targetPlayerFleetSize = existingUserFleet.size;
		}
		value newFleetSize = targetPlayerFleetSize.subtract(fleet.size);
		if (exists newFleetSize) {
			value newFleetsMap = HashMap { *fleets.coalesced };
			newFleetsMap.put(fleet.owner, Fleet(fleet.owner, newFleetSize));
			return FleetsMap(newFleetsMap);
		} else {
			return null;
		}
	}
	
	shared Fleet? getFleet(User owner) {
		return fleets.get(owner);
	}
	
	shared FleetsMap add(FleetsMap otherFleetsMap) {
		value newMap = HashMap { entries = fleets.coalesced; };
		for (user->fleet in otherFleetsMap.fleets) {
			value otherFleetSize = fleet.size; 
			value thisFleetSize = (getFleet(user) else Fleet(user, FleetSize(0))).size;
			newMap.put(user, Fleet(user, thisFleetSize.add(otherFleetSize)));
		}
		return FleetsMap(newMap);
	}
	
	shared Fleet? largestFleet {
		return fleetsMap.items.max((Fleet x, Fleet y) => x.size.compare(y.size));
	}
	
	shared actual String string => fleetsMap.map((User->Fleet element) => element.key.name->element.item.size.integerValue).string;
}
